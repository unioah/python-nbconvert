%global _empty_manifest_terminate_build 0
Name:		python-nbconvert
Version:	6.5.0
Release:	1
Summary:	Converting Jupyter Notebooks
License:	BSD
URL:		https://jupyter.org
Source0:	https://files.pythonhosted.org/packages/71/94/7b4fcc2f413a0f6cd2befe31da4c7df0eb4978e5a21a7aaddb008bba7e54/nbconvert-6.5.0.tar.gz
BuildArch:	noarch

Requires:	python3-beautifulsoup4
Requires:	python3-bleach
Requires:	python3-defusedxml
Requires:	python3-entrypoints
Requires:	python3-jinja2
Requires:	python3-jupyter-core
Requires:	python3-jupyterlab-pygments
Requires:	python3-MarkupSafe
Requires:	python3-mistune
Requires:	python3-nbclient
Requires:	python3-nbformat
Requires:	python3-packaging
Requires:	python3-pandocfilters
Requires:	python3-pygments
Requires:	python3-tinycss2
Requires:	python3-traitlets
Requires:	python3-pytest
Requires:	python3-pytest-cov
Requires:	python3-pytest-dependency
Requires:	python3-ipykernel
Requires:	python3-ipywidgets
Requires:	python3-pre-commit
Requires:	python3-pyppeteer
Requires:	python3-tornado
Requires:	python3-sphinx
Requires:	python3-sphinx-rtd-theme
Requires:	python3-nbsphinx
Requires:	python3-ipython
Requires:	python3-sphinx
Requires:	python3-sphinx-rtd-theme
Requires:	python3-nbsphinx
Requires:	python3-ipython
Requires:	python3-tornado
Requires:	python3-pytest
Requires:	python3-pytest-cov
Requires:	python3-pytest-dependency
Requires:	python3-ipykernel
Requires:	python3-ipywidgets
Requires:	python3-pre-commit
Requires:	python3-pyppeteer
Requires:	python3-pyppeteer

%description
# nbconvert

### Jupyter Notebook Conversion

[![Google Group](https://img.shields.io/badge/-Google%20Group-lightgrey.svg)](https://groups.google.com/forum/#!forum/jupyter)
[![Build Status](https://travis-ci.org/jupyter/nbconvert.svg?branch=main)](https://travis-ci.org/jupyter/nbconvert)
[![Documentation Status](https://readthedocs.org/projects/nbconvert/badge/?version=latest)](https://nbconvert.readthedocs.io/en/latest/?badge=latest)
[![Documentation Status](https://readthedocs.org/projects/nbconvert/badge/?version=stable)](https://nbconvert.readthedocs.io/en/stable/?badge=stable)
[![codecov.io](https://codecov.io/github/jupyter/nbconvert/coverage.svg?branch=main)](https://codecov.io/github/jupyter/nbconvert?branch=main)
[![CircleCI Docs Status](https://circleci.com/gh/jupyter/nbconvert/tree/main.svg?style=svg)](https://circleci.com/gh/jupyter/nbconvert/tree/main)

The **nbconvert** tool, `jupyter nbconvert`, converts notebooks to various other
formats via [Jinja][] templates. The nbconvert tool allows you to convert an
`.ipynb` notebook file into various static formats including:

- HTML
- LaTeX
- PDF
- Reveal JS
- Markdown (md)
- ReStructured Text (rst)
- executable script

## Usage

From the command line, use nbconvert to convert a Jupyter notebook (_input_) to a
a different format (_output_). The basic command structure is:

    $ jupyter nbconvert --to <output format> <input notebook>

where `<output format>` is the desired output format and `<input notebook>` is the
filename of the Jupyter notebook.

### Example: Convert a notebook to HTML

Convert Jupyter notebook file, `mynotebook.ipynb`, to HTML using:

    $ jupyter nbconvert --to html mynotebook.ipynb

This command creates an HTML output file named `mynotebook.html`.

## Dev Install

Check if pandoc is installed (`pandoc --version`); if needed, install:

```
sudo apt-get install pandoc
```

Or

```
brew install pandoc
```

Install nbconvert for development using:

```
git clone https://github.com/jupyter/nbconvert.git
cd nbconvert
pip install -e .
```

Running the tests after a dev install above:

```
pip install nbconvert[test]
py.test --pyargs nbconvert
```

## Documentation

- [Documentation for Jupyter nbconvert](https://nbconvert.readthedocs.io/en/latest/)
  [[PDF](https://media.readthedocs.org/pdf/nbconvert/latest/nbconvert.pdf)]
- [nbconvert examples on GitHub](https://github.com/jupyter/nbconvert-examples)
- [Documentation for Project Jupyter](https://jupyter.readthedocs.io/en/latest/index.html)
  [[PDF](https://media.readthedocs.org/pdf/jupyter/latest/jupyter.pdf)]

## Technical Support

- [Issues and Bug Reports](https://github.com/jupyter/nbconvert/issues): A place to report
  bugs or regressions found for nbconvert
- [Community Technical Support and Discussion - Discourse](https://discourse.jupyter.org/): A place for
  installation, configuration, and troubleshooting assistannce by the Jupyter community.
  As a non-profit project and maintainers who are primarily volunteers, we encourage you
  to ask questions and share your knowledge on Discourse.

## Jupyter Resources

- [Jupyter mailing list](https://groups.google.com/forum/#!forum/jupyter)
- [Project Jupyter website](https://jupyter.org)

[jinja]: http://jinja.pocoo.org/




%package -n python3-nbconvert
Summary:	Converting Jupyter Notebooks
Provides:	python-nbconvert
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-nbconvert
# nbconvert

### Jupyter Notebook Conversion

[![Google Group](https://img.shields.io/badge/-Google%20Group-lightgrey.svg)](https://groups.google.com/forum/#!forum/jupyter)
[![Build Status](https://travis-ci.org/jupyter/nbconvert.svg?branch=main)](https://travis-ci.org/jupyter/nbconvert)
[![Documentation Status](https://readthedocs.org/projects/nbconvert/badge/?version=latest)](https://nbconvert.readthedocs.io/en/latest/?badge=latest)
[![Documentation Status](https://readthedocs.org/projects/nbconvert/badge/?version=stable)](https://nbconvert.readthedocs.io/en/stable/?badge=stable)
[![codecov.io](https://codecov.io/github/jupyter/nbconvert/coverage.svg?branch=main)](https://codecov.io/github/jupyter/nbconvert?branch=main)
[![CircleCI Docs Status](https://circleci.com/gh/jupyter/nbconvert/tree/main.svg?style=svg)](https://circleci.com/gh/jupyter/nbconvert/tree/main)

The **nbconvert** tool, `jupyter nbconvert`, converts notebooks to various other
formats via [Jinja][] templates. The nbconvert tool allows you to convert an
`.ipynb` notebook file into various static formats including:

- HTML
- LaTeX
- PDF
- Reveal JS
- Markdown (md)
- ReStructured Text (rst)
- executable script

## Usage

From the command line, use nbconvert to convert a Jupyter notebook (_input_) to a
a different format (_output_). The basic command structure is:

    $ jupyter nbconvert --to <output format> <input notebook>

where `<output format>` is the desired output format and `<input notebook>` is the
filename of the Jupyter notebook.

### Example: Convert a notebook to HTML

Convert Jupyter notebook file, `mynotebook.ipynb`, to HTML using:

    $ jupyter nbconvert --to html mynotebook.ipynb

This command creates an HTML output file named `mynotebook.html`.

## Dev Install

Check if pandoc is installed (`pandoc --version`); if needed, install:

```
sudo apt-get install pandoc
```

Or

```
brew install pandoc
```

Install nbconvert for development using:

```
git clone https://github.com/jupyter/nbconvert.git
cd nbconvert
pip install -e .
```

Running the tests after a dev install above:

```
pip install nbconvert[test]
py.test --pyargs nbconvert
```

## Documentation

- [Documentation for Jupyter nbconvert](https://nbconvert.readthedocs.io/en/latest/)
  [[PDF](https://media.readthedocs.org/pdf/nbconvert/latest/nbconvert.pdf)]
- [nbconvert examples on GitHub](https://github.com/jupyter/nbconvert-examples)
- [Documentation for Project Jupyter](https://jupyter.readthedocs.io/en/latest/index.html)
  [[PDF](https://media.readthedocs.org/pdf/jupyter/latest/jupyter.pdf)]

## Technical Support

- [Issues and Bug Reports](https://github.com/jupyter/nbconvert/issues): A place to report
  bugs or regressions found for nbconvert
- [Community Technical Support and Discussion - Discourse](https://discourse.jupyter.org/): A place for
  installation, configuration, and troubleshooting assistannce by the Jupyter community.
  As a non-profit project and maintainers who are primarily volunteers, we encourage you
  to ask questions and share your knowledge on Discourse.

## Jupyter Resources

- [Jupyter mailing list](https://groups.google.com/forum/#!forum/jupyter)
- [Project Jupyter website](https://jupyter.org)

[jinja]: http://jinja.pocoo.org/




%package help
Summary:	Development documents and examples for nbconvert
Provides:	python3-nbconvert-doc
%description help
# nbconvert

### Jupyter Notebook Conversion

[![Google Group](https://img.shields.io/badge/-Google%20Group-lightgrey.svg)](https://groups.google.com/forum/#!forum/jupyter)
[![Build Status](https://travis-ci.org/jupyter/nbconvert.svg?branch=main)](https://travis-ci.org/jupyter/nbconvert)
[![Documentation Status](https://readthedocs.org/projects/nbconvert/badge/?version=latest)](https://nbconvert.readthedocs.io/en/latest/?badge=latest)
[![Documentation Status](https://readthedocs.org/projects/nbconvert/badge/?version=stable)](https://nbconvert.readthedocs.io/en/stable/?badge=stable)
[![codecov.io](https://codecov.io/github/jupyter/nbconvert/coverage.svg?branch=main)](https://codecov.io/github/jupyter/nbconvert?branch=main)
[![CircleCI Docs Status](https://circleci.com/gh/jupyter/nbconvert/tree/main.svg?style=svg)](https://circleci.com/gh/jupyter/nbconvert/tree/main)

The **nbconvert** tool, `jupyter nbconvert`, converts notebooks to various other
formats via [Jinja][] templates. The nbconvert tool allows you to convert an
`.ipynb` notebook file into various static formats including:

- HTML
- LaTeX
- PDF
- Reveal JS
- Markdown (md)
- ReStructured Text (rst)
- executable script

## Usage

From the command line, use nbconvert to convert a Jupyter notebook (_input_) to a
a different format (_output_). The basic command structure is:

    $ jupyter nbconvert --to <output format> <input notebook>

where `<output format>` is the desired output format and `<input notebook>` is the
filename of the Jupyter notebook.

### Example: Convert a notebook to HTML

Convert Jupyter notebook file, `mynotebook.ipynb`, to HTML using:

    $ jupyter nbconvert --to html mynotebook.ipynb

This command creates an HTML output file named `mynotebook.html`.

## Dev Install

Check if pandoc is installed (`pandoc --version`); if needed, install:

```
sudo apt-get install pandoc
```

Or

```
brew install pandoc
```

Install nbconvert for development using:

```
git clone https://github.com/jupyter/nbconvert.git
cd nbconvert
pip install -e .
```

Running the tests after a dev install above:

```
pip install nbconvert[test]
py.test --pyargs nbconvert
```

## Documentation

- [Documentation for Jupyter nbconvert](https://nbconvert.readthedocs.io/en/latest/)
  [[PDF](https://media.readthedocs.org/pdf/nbconvert/latest/nbconvert.pdf)]
- [nbconvert examples on GitHub](https://github.com/jupyter/nbconvert-examples)
- [Documentation for Project Jupyter](https://jupyter.readthedocs.io/en/latest/index.html)
  [[PDF](https://media.readthedocs.org/pdf/jupyter/latest/jupyter.pdf)]

## Technical Support

- [Issues and Bug Reports](https://github.com/jupyter/nbconvert/issues): A place to report
  bugs or regressions found for nbconvert
- [Community Technical Support and Discussion - Discourse](https://discourse.jupyter.org/): A place for
  installation, configuration, and troubleshooting assistannce by the Jupyter community.
  As a non-profit project and maintainers who are primarily volunteers, we encourage you
  to ask questions and share your knowledge on Discourse.

## Jupyter Resources

- [Jupyter mailing list](https://groups.google.com/forum/#!forum/jupyter)
- [Project Jupyter website](https://jupyter.org)

[jinja]: http://jinja.pocoo.org/




%prep
%autosetup -n nbconvert-6.5.0

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/share ]; then
	find usr/share -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-nbconvert -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Apr 25 2022 Python_Bot <Python_Bot@openeuler.org> - 6.5.0-1
- Package Spec generated
